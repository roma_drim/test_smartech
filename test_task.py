from selenium import webdriver
import unittest
import HtmlTestRunner
import time
import re

class StatNew(unittest.TestCase):

    def setUp(self):
        profile = webdriver.ChromeOptions()
        self.driver = webdriver.Chrome(options=profile)
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()

    def test_script(self):
        self.driver.get('https://yandex.ru/')
        self.driver.find_element_by_xpath('//*[@id="text"]').send_keys('расчет расстояний между городами')
        self.driver.find_element_by_class_name('search2__button').click()
        links = self.driver.find_elements_by_link_text('Расчет расстояний между городами')
        # Перебираем все вкладки с названием "Расчет расстояний между городами", пока не найдем нужную нам ссылку
        for i in range (len(links)):
            if (links[i].get_attribute('href') == 'https://www.avtodispetcher.ru/distance/'): links[i].click()
        # Получаем все открытые вкладки
        windows = self.driver.window_handles
        # Переключаемся на нужную нам вкладку
        self.driver.switch_to.window(window_name=windows[1])
        from_field = self.driver.find_element_by_xpath('//*[@id="from_field_parent"]/input')
        from_field.clear()
        from_field.send_keys('Тула')
        to_filed = self.driver.find_element_by_xpath('//*[@id="to_field_parent"]/input')
        to_filed.clear()
        to_filed.send_keys('Санкт-Петербург')
        expense = self.driver.find_element_by_xpath('//*[@id="CalculatorForm"]/div[2]/div[1]/label/input')
        expense.clear()
        expense.send_keys('9')
        price = self.driver.find_element_by_xpath('//*[@id="CalculatorForm"]/div[2]/div[2]/label/input')
        price.clear()
        price.send_keys('46')
        self.driver.find_element_by_xpath('//*[@id="CalculatorForm"]/div[3]/input').submit()
        time.sleep(3)
        distance = self.driver.find_element_by_xpath('//*[@id="totalDistance"]').text
        # С помощью библиотеки unittest сравниваем ожидаемый и полученный результат
        self.assertEqual(distance, '897')
        # С помощью библиотеки re выдергиваем из строки число
        cost = re.findall('(\d+)', self.driver.find_element_by_xpath('//*[@id="summaryContainer"]/div/div[2]/a').text)
        self.assertEqual(cost[0], '3726')
        self.driver.find_element_by_xpath('//*[@id="triggerFormD"]').click()
        self.driver.find_element_by_xpath('//*[@id="inter_points_field_parent"]/input').send_keys('Великий Новгород')
        time.sleep(60)
        self.driver.find_element_by_xpath('//*[@id="CalculatorForm"]/div[2]/input').submit()
        time.sleep(3)
        distance = self.driver.find_element_by_xpath('//*[@id="totalDistance"]').text
        self.assertEqual(distance, '966')
        cost = re.findall('(\d+)', self.driver.find_element_by_xpath('//*[@id="summaryContainer"]/div/div[2]/a').text)
        self.assertEqual(cost[0], '4002')


if __name__ == '__main__':
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output='reports'))
